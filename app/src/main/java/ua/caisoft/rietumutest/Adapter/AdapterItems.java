package ua.caisoft.rietumutest.Adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ua.caisoft.rietumutest.R;
import ua.caisoft.rietumutest.Realm.models.ItemModel;

public class AdapterItems extends ArrayAdapter<ItemModel> {
    final LayoutInflater mInflater;
    private Context mContext;
    public AdapterItems(Context context) {
        super(context, 0);
        mInflater = LayoutInflater.from(getContext());
        this.mContext=context;
    }

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {

        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            holder = new ViewHolder();
            rowView =  mInflater.inflate(R.layout.item_adapter, null, false);
            holder.info_la = (LinearLayout) rowView.findViewById(R.id.info_la);
            holder.buyUSD = (TextView) rowView.findViewById(R.id.buyUSD);
            holder.buyEUR = (TextView) rowView.findViewById(R.id.buyEUR);
            holder.imgUSD = (ImageView) rowView.findViewById(R.id.imgUSD);
            holder.imgEUR = (ImageView) rowView.findViewById(R.id.imgEUR);
            holder.name = (TextView) rowView.findViewById(R.id.name);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        final ItemModel item = getItem(position);
        Picasso
                .with(getContext())
                .load(R.drawable.dollar)
                .centerCrop()
                .fit()
                .into(holder.imgUSD);
        Picasso
                .with(getContext())
                .load(R.drawable.euro)
                .centerCrop()
                .fit()
                .into(holder.imgEUR);

        if(!TextUtils.isEmpty(item.getName())){
            holder.name.setText(item.getName());
        }
        if(!TextUtils.isEmpty(item.getBuyEUR())){
            holder.buyEUR.setText(item.getBuyEUR());
        }
        if(!TextUtils.isEmpty(item.getBuyUSD())){
            holder.buyUSD.setText(String.valueOf(item.getBuyUSD()));
        }
        return rowView;
    }

    static class ViewHolder {
        public LinearLayout info_la;
        public TextView buyUSD;
        public TextView buyEUR;
        public TextView name;
        public ImageView imgUSD;
        public ImageView imgEUR;
    }

}
