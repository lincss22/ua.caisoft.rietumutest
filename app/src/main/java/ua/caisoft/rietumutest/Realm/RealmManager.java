package ua.caisoft.rietumutest.Realm;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import ua.caisoft.rietumutest.Realm.models.ItemModel;
import ua.caisoft.rietumutest.Realm.models.MainModel;
import ua.caisoft.rietumutest.Utils.MainApplication_;

public class RealmManager {
    private static Context mContext;
    private static RealmManager mInstance;

    public static RealmManager getInstance() {
        mContext = MainApplication_.getInstance().getApplicationContext();
        synchronized (RealmManager.class) {
            if (mInstance == null) {
                mInstance = new RealmManager();
            }
        }
        return mInstance;
    }

    public void saveMain(MainModel model) {
        Realm realm = null;
        try {
            if (TextUtils.isEmpty(model.getId())) {
                model.setId(UUID.randomUUID() + "");
            }
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }
    public void saveItemList(ItemModel model) {
        Realm realm = null;
        try {
            if (TextUtils.isEmpty(model.getId())) {
                model.setId(UUID.randomUUID() + "");
            }
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public ArrayList<MainModel> getItemProfiles() {
        ArrayList<MainModel> model = null;
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            RealmResults<MainModel> realmResults = realm
                    .where(MainModel.class)
                    .findAll();
            realm.commitTransaction();
            model = cloneItems(realmResults);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return model;
    }

    private ArrayList<MainModel> cloneItems(RealmResults<MainModel> model) {
        ArrayList<MainModel> address = new ArrayList<>();
        for (MainModel item : model) {
            address.add(clone(item));
        }
        return address;
    }

    private MainModel clone(MainModel model) {
        try {
            MainModel address = new MainModel();
            address.setId(model.getId());
            address.setItems(cloneItems(model.getItems()));
            address.setDate(model.getDate());
            return address;
        } catch (Exception ex) {
            return null;
        }
    }

    private ItemModel clone(ItemModel model) {
        ItemModel address = new ItemModel();
        address.setId(model.getId());
        address.setBuyUSD(model.getBuyUSD());
        address.setBuyEUR(model.getBuyEUR());
        address.setName(model.getName());
        return address;
    }

    private RealmList<ItemModel> cloneItems(RealmList<ItemModel> model) {
        RealmList<ItemModel> address = new RealmList<>();
        for (ItemModel item : model) {
            address.add(clone(item));
        }
        return address;
    }

}