package ua.caisoft.rietumutest.Realm.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ItemModel extends RealmObject{
    @PrimaryKey
    private String id;

    private String name;
    private String buyUSD;
    private String buyEUR;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBuyUSD() {
        return buyUSD;
    }

    public void setBuyUSD(String buyUSD) {
        this.buyUSD = buyUSD;
    }

    public String getBuyEUR() {
        return buyEUR;
    }

    public void setBuyEUR(String buyEUR) {
        this.buyEUR = buyEUR;
    }

}
