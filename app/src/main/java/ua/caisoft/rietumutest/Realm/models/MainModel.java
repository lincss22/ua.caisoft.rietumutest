package ua.caisoft.rietumutest.Realm.models;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MainModel extends RealmObject implements Serializable {
    @PrimaryKey
    private String id;

    private RealmList<ItemModel> items;
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RealmList<ItemModel> getItems() {
        return items;
    }

    public void setItems(RealmList<ItemModel> items) {
        this.items = items;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
