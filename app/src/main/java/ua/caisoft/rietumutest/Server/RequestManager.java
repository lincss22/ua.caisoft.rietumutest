package ua.caisoft.rietumutest.Server;


import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import ua.caisoft.rietumutest.Server.ServerModels.ResponseItems;

public class RequestManager {
    private static RequestManager mInstance;
    private final Transport.TransportService mService;
    private RequestManager() {
        mService = TransportHolder.getServiceInstance();
    }

    public static RequestManager getInstance() {
        synchronized (RequestManager.class) {
            if(mInstance == null) {
                mInstance = new RequestManager();
            }
        }
        return mInstance;
    }
    public ResponseBody getItems(){
        try {
            return mService.getItems("no","rdate",220917,"yesrdate").execute().body();
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}

