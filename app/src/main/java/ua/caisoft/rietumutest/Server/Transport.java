package ua.caisoft.rietumutest.Server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ua.caisoft.rietumutest.Server.ServerModels.ResponseItems;

public class Transport {
    public static final String HOST = "http://www.rietumu.ru";
    public static final String BASE_URL = HOST;
    public interface TransportService {
        @GET("/sng.nsf/RatesXML")
        Call<ResponseBody> getItems(
                @Query("OpenAgent") String openagent,
                @Query("cash") String cash,
                @Query("rdate") long rdate,
                @Query("exdate") String exdate);

    }

    static TransportService newInstance() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(TransportService.class);
    }
}