package ua.caisoft.rietumutest.Activity;

import android.app.ProgressDialog;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import net.danlew.android.joda.DateUtils;
import net.danlew.android.joda.JodaTimeAndroid;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.realm.RealmList;
import okhttp3.ResponseBody;
import ua.caisoft.rietumutest.Adapter.AdapterItems;
import ua.caisoft.rietumutest.R;
import ua.caisoft.rietumutest.Realm.RealmManager;
import ua.caisoft.rietumutest.Realm.models.ItemModel;
import ua.caisoft.rietumutest.Realm.models.MainModel;
import ua.caisoft.rietumutest.Server.RequestManager;
import ua.caisoft.rietumutest.Server.ServerModels.ItemsServerModel;
import ua.caisoft.rietumutest.Server.ServerModels.ResponseItems;


@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    @ViewById
    ListView list;
    @ViewById
    TextView date;
    @ViewById
    TextView currentTime;
    @ViewById
    LinearLayout refresh;
    AdapterItems adapter;
    public ProgressDialog progressDialog;

    @Override
    protected void onResume() {
        super.onResume();
        if(RealmManager.getInstance().getItemProfiles() != null && !RealmManager.getInstance().getItemProfiles().isEmpty()){
            for(MainModel m:RealmManager.getInstance().getItemProfiles()){
                fillItems(m);
            }
        }
    }

    @AfterViews
    public void initActivity(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.please_wait_message);
        progressDialog.setCancelable(false);
        progressDialog.show();
        loadGetItems();
    }

    void fillItems(MainModel mainModel){
        adapter = new AdapterItems(this);
        adapter.addAll(mainModel.getItems());
        list.setAdapter(adapter);
        if(!TextUtils.isEmpty(mainModel.getDate())){
            date.setText(mainModel.getDate());
        }
        adapter.sort(new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel arg1, ItemModel arg0) {
                return arg1.getName().compareToIgnoreCase(arg0.getName());
            }
        });
        currentTime();
        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }
    void currentTime(){
        CountDownTimer newtimer = new CountDownTimer(1000000000, 1000) {
            public void onTick(long millisUntilFinished) {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");
                DateTime dt = DateTime.now();
                currentTime.setText(String.valueOf(formatter.print(dt)));
            }
            public void onFinish() {

            }
        };
        newtimer.start();
    }

    void savingToRealm(ResponseItems responseItems){
        if(responseItems.items != null && !responseItems.items.isEmpty()){
            MainModel mainModel = new MainModel();
            RealmList<ItemModel> itemsToSave = new RealmList<>();
            for(ItemsServerModel i:responseItems.items){
                ItemModel itemModel = new ItemModel();
                itemModel.setName(i.name);
                itemModel.setBuyEUR(i.buyEUR);
                itemModel.setBuyUSD(i.buyUSD);
                itemsToSave.add(itemModel);
                RealmManager.getInstance().saveItemList(itemModel);
            }
            mainModel.setItems(itemsToSave);
            String time = timeWithLocale(responseItems.date);
            mainModel.setDate(time);
            RealmManager.getInstance().saveMain(mainModel);
            fillItems(mainModel);
        } else {
            if(RealmManager.getInstance().getItemProfiles() != null && !RealmManager.getInstance().getItemProfiles().isEmpty()){
                ArrayList<MainModel> mainModels = RealmManager.getInstance().getItemProfiles();
                // MainModel сделана в множественном числе для дальнейшей легкости кастомизации (на всякий случай)
                for(MainModel m:mainModels){
                    fillItems(m);
                }
            } else {
                Toast.makeText(this,"Some error with network connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    String timeWithLocale(String t){
        String time = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTF"));
        Date convertedDate = null;
        try {
            convertedDate = sdf.parse(t);
            DateTime dateTime = new DateTime(convertedDate);
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy HH:mm");
            return time = String.valueOf(formatter.print(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    @Background
    void loadGetItems() {
        onGetItems(RequestManager.getInstance().getItems());
    }

    @UiThread
    void onGetItems(ResponseBody responseItems){
        if(responseItems != null){
            try {
                String s = responseItems.string();
                s = s.replace("var nbRate=","");
                Gson gson = new Gson();
                JsonReader reader = new JsonReader(new StringReader(s));
                reader.setLenient(true);
                ResponseItems response = gson.fromJson(reader, ResponseItems.class);
                //TODO: Для проверки на нуловость курсов валют раскоментить 171 строку
                //response.items = null;
                savingToRealm(response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if(progressDialog != null){
                progressDialog.dismiss();
            }
            Toast.makeText(this,"Some error with network connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Click
    void refresh(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.please_wait_message);
        progressDialog.setCancelable(false);
        progressDialog.show();
        loadGetItems();
    }
}
